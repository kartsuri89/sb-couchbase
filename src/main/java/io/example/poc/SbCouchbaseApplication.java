package io.example.poc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

@SpringBootApplication(exclude = SecurityAutoConfiguration.class, proxyBeanMethods = false)
public class SbCouchbaseApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(SbCouchbaseApplication.class, args);
	}

}
