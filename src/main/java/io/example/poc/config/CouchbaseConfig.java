package io.example.poc.config;

import java.time.Duration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.couchbase.config.AbstractCouchbaseConfiguration;

import com.couchbase.client.core.error.BucketNotFoundException;
import com.couchbase.client.core.error.UnambiguousTimeoutException;
import com.couchbase.client.core.msg.kv.DurabilityLevel;
import com.couchbase.client.java.Bucket;
import com.couchbase.client.java.Cluster;
import com.couchbase.client.java.manager.bucket.BucketSettings;
import com.couchbase.client.java.manager.bucket.BucketType;

@Configuration
public class CouchbaseConfig extends AbstractCouchbaseConfiguration {

	Logger log = LoggerFactory.getLogger(CouchbaseConfig.class);

	@Value("${spring.couchbase.connection-string:localhost}")
	private String connectionString;
	
	@Value("${spring.data.couchbase.bucket-name:default}")
	private String bucketName;
	
	@Value("${spring.couchbase.username:Administrator}")
	private String username;
	
	@Value("${spring.couchbase.password:password}")
	private String password;
	
	@Override
	public String getConnectionString() {
		return connectionString;
	}

	@Override
	public String getUserName() {
		return username;
	}

	@Override
	public String getPassword() {
		return password;
	}

	@Override
	public String getBucketName() {
		return bucketName;
	}
	
	@Override
	protected boolean autoIndexCreation() {
		return true;
	}
	
	//@Bean(destroyMethod = "disconnect")
    Cluster getCouchbaseCluster() {
        try {
            log.debug("Connecting to Couchbase cluster at {} ", connectionString);
            Cluster cluster = Cluster.connect(connectionString, username, password);
            cluster.waitUntilReady(Duration.ofSeconds(15));
            return cluster;
        } catch (UnambiguousTimeoutException e) {
            log.error("Connection to Couchbase cluster at {} timed out", connectionString);
            throw e;
        } catch (Exception e) {
            log.error(e.getClass().getName());
            log.error("Could not connect to Couchbase cluster at {}", connectionString);
            throw e;
        }

    }

    @Bean
    Bucket getCouchbaseBucket(Cluster cluster) {
    	log.info("::::::::::Inside creation of Bucket:::::::::::");
    	log.info("bucket available::: {}", cluster.buckets().getAllBuckets());
        try {
            if (!cluster.buckets().getAllBuckets().containsKey(bucketName)) {
            	log.info("{} bucket Not available - So creating ",bucketName);
            	cluster.buckets().createBucket(
                        BucketSettings.create(bucketName)
                            .bucketType(BucketType.COUCHBASE)
                            .minimumDurabilityLevel(DurabilityLevel.NONE)
                            .ramQuotaMB(128));
                
                cluster.bucket(bucketName).waitUntilReady(Duration.ofSeconds(10));              
            } else {
            	log.info("Wowwww...! Bucket : '{}' already available in cluster ...!!",bucketName);
            }
            return cluster.bucket(bucketName);
        } catch (UnambiguousTimeoutException e) {
            log.error("Connection to bucket {} timed out",bucketName);
            throw e;
        } catch (BucketNotFoundException e) {
            log.error("Bucket {} does not exist",bucketName);
            throw e;
        } catch (Exception e) {
            log.error(e.getClass().getName());
            log.error("Could not connect to bucket {}", bucketName);
            throw e;
        }
    }
}
