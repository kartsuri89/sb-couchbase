package io.example.poc.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.couchbase.core.mapping.event.ValidatingCouchbaseEventListener;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

@Configuration
public class Config {
	
	Logger log = LoggerFactory.getLogger(Config.class);

	//@PostConstruct
	/*private void postConstruct() {
		System.out.println("Bucket Name :::::::"+couchbaseTemplate.getBucketName());
		cluster.queryIndexes().createPrimaryIndex(couchbaseTemplate.getBucketName(),
				CreatePrimaryQueryIndexOptions.createPrimaryQueryIndexOptions().ignoreIfExists(true));

		// Need to post-process travel data to add _class attribute
		cluster.query("update `travel-sample` set _class='" + User.class.getName() + "' where type = 'airline'");
	}*/

	@Bean
	LocalValidatorFactoryBean localValidatorFactoryBean() {
		return new LocalValidatorFactoryBean();
	}
	
	@Bean
	ValidatingCouchbaseEventListener validatingCouchbaseEventListener() {
		return new ValidatingCouchbaseEventListener(localValidatorFactoryBean());
	}
}
