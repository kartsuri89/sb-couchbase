package io.example.poc.controller;

import java.util.ArrayList;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.example.poc.model.ResultDTO;
import io.example.poc.model.User;
import io.example.poc.service.UserService;

@RestController
@RequestMapping(path = "/api/v1")
public class UserController {
	
	Logger log = LoggerFactory.getLogger(UserController.class);

	@Autowired
	private UserService userService;

	@PostMapping("/createUser")
	public ResponseEntity<?> createUser(@RequestBody User reqData) {
		log.info(":::  UserController.createUser :::");
		ResultDTO<?> responsePacket = null;
		try {
			//ArrayList<String> errorList = beanValidator.userValidate(reqData);
			ArrayList<String> errorList = new ArrayList<>();
			if (!errorList.isEmpty()) {
				ResultDTO<ArrayList<String>> errorPacket = new ResultDTO<>(errorList,
						"Above fields value must not be empty", false);
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorPacket);
			}
			Optional<User> isData = userService.isDataExist(reqData);
			if (!isData.isPresent()) {
				responsePacket = new ResultDTO<>(userService.createUser(reqData), "User Created Successfully", true);
				return ResponseEntity.ok(responsePacket);
			} else {
				responsePacket = new ResultDTO<>("Record already exist", false);
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responsePacket);
			}
		} catch (Exception e) {
			responsePacket = new ResultDTO<>(e.getMessage(), false);
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responsePacket);
		}
	}

	@GetMapping("/allUsers")
	public ResponseEntity<?> allUsers() {
		log.info(":::  UserController.allUsers :::");
		ResultDTO<?> responsePacket = null;
		try {
			responsePacket = new ResultDTO<>(userService.getAllUsers(), "Users fetched successfully !!", true);
			return ResponseEntity.ok(responsePacket);
		} catch (Exception e) {
			responsePacket = new ResultDTO<>(e.getMessage(), false);
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responsePacket);
		}
	}

	@GetMapping("/getUserById/{id}")
	public ResponseEntity<?> getUserById(@PathVariable("id") String id) {
		log.info(":::  UserController.getUserById :::");
		ResultDTO<?> responsePacket = null;
		try {
			responsePacket = new ResultDTO<>(userService.getUserById(id), "User fetched successfully !!", true);
			return ResponseEntity.ok(responsePacket);
		} catch (Exception e) {
			responsePacket = new ResultDTO<>(e.getMessage(), false);
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responsePacket);
		}
	}

	@PutMapping("/updateUser")
	public ResponseEntity<?> updateUser(@RequestBody User reqData) {
		log.info(":::  UserController.updateUser :::");
		ResultDTO<?> responsePacket = null;
		try {
			Optional<User> isData = userService.findUserById(reqData.getId());
			if (isData.isPresent()) {
				responsePacket = new ResultDTO<>(userService.updateUser(reqData, isData.get()),
						"User Updated Successfully", true);
				return ResponseEntity.ok(responsePacket);
			} else {
				responsePacket = new ResultDTO<>("Record not exist", false);
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responsePacket);
			}
		} catch (Exception e) {
			responsePacket = new ResultDTO<>(e.getMessage(), false);
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responsePacket);
		}
	}

	@DeleteMapping("/deleteUserById/{id}")
	public ResponseEntity<?> deleteUserById(@PathVariable("id") String id) {
		log.info(":::  UserController.deleteUserById :::");
		ResultDTO<?> responsePacket = null;
		try {
			responsePacket = new ResultDTO<>(userService.deleteUserById(id), "User deleted successfully !!", true);
			return ResponseEntity.ok(responsePacket);
		} catch (Exception e) {
			responsePacket = new ResultDTO<>(e.getMessage(), false);
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responsePacket);
		}
	}
}
