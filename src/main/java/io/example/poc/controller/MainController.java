package io.example.poc.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import io.example.poc.service.UserService;

@RestController
public class MainController {

	Logger log = LoggerFactory.getLogger(MainController.class);

	UserService userService;
	
	public MainController(UserService userService) {
		super();
		this.userService = userService;
	}

	@GetMapping("/")
	public String get() {
		log.info("Inside MainController");
		return "Hello World from SpringBoot + CouchBase..!!!";
	}
}
