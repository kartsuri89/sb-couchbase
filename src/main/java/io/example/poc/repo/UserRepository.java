package io.example.poc.repo;

import java.util.Optional;

import org.springframework.data.couchbase.repository.CouchbaseRepository;
import io.example.poc.model.User;

public interface UserRepository extends CouchbaseRepository<User, String> {

	Optional<User> findByEmailAndMobNo(String email, String mobNo);
}
