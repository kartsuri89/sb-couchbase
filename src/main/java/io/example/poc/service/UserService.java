package io.example.poc.service;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import io.example.poc.model.User;
import io.example.poc.repo.UserRepository;

@Service
public class UserService {
	
	Logger log = LoggerFactory.getLogger(UserService.class);

    private UserRepository userRepo;
	
	public UserService(UserRepository userRepository) {
		super();
		this.userRepo = userRepository;
	}

	public Object createUser(User reqData) {
		return userRepo.save(reqData);
	}

	public Object getAllUsers() {
		return userRepo.findAll();
	}

	public Optional<User> findUserById(String id) {
		return userRepo.findById(id);
	}

	public Object getUserById(String id) {
		return userRepo.findById(id);
	}

	public Object updateUser(User reqData, User isData) {
		isData.setName(reqData.getName());
		isData.setEmail(reqData.getEmail());
		isData.setMobNo(reqData.getMobNo());
		isData.setPassword(reqData.getPassword());
		return userRepo.save(isData);
	}

	public Object deleteUserById(String id) {
		userRepo.deleteById(id);
		return null;
	}

	public Optional<User> isDataExist(User reqData) {
		return userRepo.findByEmailAndMobNo(reqData.getEmail(), reqData.getMobNo());
	}
}
