package io.example.poc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.test.context.TestConfiguration;

@TestConfiguration(proxyBeanMethods = false)
public class TestSbCouchbaseApplication {

	public static void main(String[] args) {
		SpringApplication.from(SbCouchbaseApplication::main).with(TestSbCouchbaseApplication.class).run(args);
	}

}
