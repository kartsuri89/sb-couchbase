FROM eclipse-temurin:17-jdk-jammy
#WORKDIR /app
#ADD target/sb_couchbase-0.0.1-SNAPSHOT.jar ./app.jar
ARG JAR_FILE=target/sb_couchbase-0.0.1-SNAPSHOT.jar
COPY ${JAR_FILE} app.jar
ENTRYPOINT [ "java", "-jar", "/app.jar" ]